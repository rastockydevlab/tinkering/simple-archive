use flate2::{
    write::GzEncoder,
    Compression,
};
use std::{
    env::args,
    fs::File,
    io::{copy, BufReader},
    time::Instant,
};

fn main() {
    if args().len() != 3 {
        eprintln!("Usage: path/to/source path/to/target");
        return;
    }

    let src_file_name = args().nth(1).unwrap();
    let src_file = File::open(src_file_name).unwrap();
    let mut input = BufReader::new(src_file);

    let out_file_name = args().nth(2).unwrap();
    let output = File::create(out_file_name).unwrap();

    let mut encoder = GzEncoder::new(output, Compression::default());

    let start = Instant::now();

    copy(&mut input, &mut encoder).unwrap();

    let output = encoder.finish().unwrap();

    println!("Source len: {:?}", input.get_ref().metadata().unwrap().len());
    println!("Output len: {:?}", output.metadata().unwrap().len());
    println!("Took  time: {:?}", start.elapsed());
}
